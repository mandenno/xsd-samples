<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 
 
$url="https://nbk_validation_url";
$xml = "<?xml version='1.0' encoding='UTF-8'?>
		<validatePayment>
		<transactionId>H6526262H</transactionId>
		</validatePayment>";
 

//Initiate cURL
$curl = curl_init($url);


// Set the Content-Type to text/xml.
curl_setopt ($curl, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
 
// Set CURLOPT_POST to true to send a POST request.
curl_setopt($curl, CURLOPT_POST, true);
 
// Attach the XML string to the body of our request.
curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
 
// Tell cURL that we want the response to be returned as
// a string instead of being dumped to the output.
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
 
// Set the TLS version
curl_setopt ($curl, CURLOPT_SSLVERSION, 6);
 
// Execute the POST request and send our XML.
$result = curl_exec($curl);
 
// Do some basic error checking.
//if(curl_errno($curl)){
//    throw new Exception(curl_error($curl));
//}
 
try{
    if(curl_errno($curl)){
        throw new Exception(curl_error($curl));
    }
} catch(Exception $e){
  print("An error has occured: ".$e->getMessage());
}
 
//Close the cURL handle.
curl_close($curl);
 
//Print out the response output.
echo $result;
?>